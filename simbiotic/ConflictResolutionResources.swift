//
//  ConflictResolutionResources.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/30/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import Foundation

let typeInformation = [
    "Concede": [
        "description": "I'll agree and let them win",
        "goal": "To maintain relationships and keep the peace",
        "examples": [
        "Giving your roommate the bigger room",
        "Letting your roommate have the room along for the night",
        "Letting your roommate choose the decorations for the walls"
        ],
        "pro": "does not jeopardize potentially important",
        "con": "solutions can be unfair to you, causing you regret and resentment",
        "appropriateUses": [
        "When you realize that you are wrong and that the other persons solution is better",
        "When maintaining your relationship is more important than the issue at hand",
        "When the issue is much more important to the other person than it is to you"
        ],
        "imageName": "hand"
    ],
    "Avoid": [
        "description": "I'm not getting involved",
        "goal": "To delay conflict or avoid confrontation",
        "examples": [
        "Ignoring a mess rather than confronting your roommate about it",
        "Discussing a conflict the next day when everyone has had a chance to cool down",
        "Changing the topic when your roommate brings up and issue"
        ],
        "pro": "nobody directly gets hurt",
        "con": "unaddressed conflicts may grow worse over time",
        "appropriateUses": [
        "When someone else can resolve the conflict more effectively",
        "When you need to find more information about the issue",
        "When the costs of confrontation outweigh the benefits of quick action"
        ],
        "imageName": "stop"
    ],
    "Compete": [
        "description": "I'm going to win",
        "goal": "To win or to make your opinion known",
        "examples": [
        "Giving your roommate an ultimatum for putting away their things",
        "Stopping your roommate from taking another drink before leaving",
        "Yelling at your roommate to make him or her concede"
        ],
        "pro": "quick decisions can be made",
        "con": "higher risk of damaging relationships",
        "appropriateUses": [
        "When you know you are correct and are doing the right thing",
        "In emergencies or when safety is a concern",
        "When the issue is something on which you know you cannot compromise"
        ],
        "imageName": "Crown"
    ],
    "Collaborate": [
        "description": "We'll work together",
        "goal": "To find a win/win situation for both people by working together",
        "examples": [
        "Pooling your money with your roommate to buy a larger fridge",
        "Using both people's experience to maximize space in a room layout",
        "Connecting one person's speakers with the other person's TV to share"
        ],
        "pro": "both people often leave with the optimal solution",
        "con": "one person can possibly take advantage of the other",
        "appropriateUses": [
        "When having two opinions is better than one",
        "When you can achieve something together that you could not achieve alone",
        "When both people can commit time and energy to finding a complex solution"
        ],
        "imageName": "yinyang2"
    ],
    "Compromise": [
        "description": "I'll split the difference",
        "goal": "To find an acceptable (if not necessarily optimal) solution for both people",
        "examples": [
        "Letting your roommate have guests over only on the weekends",
        "Alternating taking out the trash by week",
        "Setting and adhering to each other's quiet hours"
        ],
        "pro": "both people can feel satisfied with the decision",
        "con": "lingering anger over a non-optimal solution can worsen over time",
        "appropriateUses": [
        "When both people can agree to disagree and live with the decision",
        "When it is unrealistic that both people will get their ideal outcome",
        "If the situation needs a solution (even a temporary one)"
        ],
        "imageName": "halfcircle"
    ],
]

