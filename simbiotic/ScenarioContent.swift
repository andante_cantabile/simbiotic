//
//  ScenarioContent.swift
//  simbiotic
//
//  Created by Ellen Roper on 12/5/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import Foundation

func getScenarioContent(scenario: Scenario) -> [String: Any] {
    switch scenario {
    case .Bottle:
        return [
            "Question": "Your roommate is drinking on a Wednesday night and is leaving bottles everywhere. What do you do?",
            "Answers": [
                "You don’t like it but will let it slide.",
                "You don’t mind. They can do what they want.",
                "You discuss and decide to only drink on weekends.",
                "You start yelling at them.",
            ]
        ]
    case .Trash:
        return [
            "Question": "Your roommate never takes out the trash - you do it every time. What do you do?",
                "Answers":[
                    "You leave it; if it gets bad enough they'll do it.",
                    "You keep doing it. They're busy and it's not too much of a hassle.",
                    "You decide to switch off each week.",
                    "You confront them about it. It's unsanitary and needs to stop.",
            ]
        ]
    case .Pizza:
        return ["Question": "Your roommate leaves old food out in your room. It's starting to rot. What do you do?",
                "Answers": [
                    "You leave it; if it's on their side of the room, you don't care.",
                    "You just throw it away; it's a waste of time to argue with them.",
                    "You come up with a system and add a compost bin to your room.",
                    "You put it on their bed. That will show them!",
            ]
        ]
    }
}

func translateStringToAnswerIndex(scenario: Scenario, string: String) -> Int {
    print("Inside translation function: Answer text is " + string)
    let content = getScenarioContent(scenario: scenario)
    let answers = content["Answers"] as! [String]
    print(answers[0] == string)
    for (index, ans) in answers.enumerated() {
        if ans == string {
            return index
        }
    }
    return -1
}

enum SurveyQuestion {
    case Hygiene
    case ShareResponsibility
    case Alcohol
}
func computeSurveyAnswer(vrAnswers: [Scenario: Int], question: SurveyQuestion) -> Float {
    let bottleNumber = vrAnswers[.Bottle]
    let trashNumber = vrAnswers[.Trash]
    let pizzaNumber = vrAnswers[.Pizza]
    switch question {
    case .Hygiene:
        return 0.5 * Float(trashNumber! + pizzaNumber!)
    case .ShareResponsibility:
        return Float(trashNumber!)
    case .Alcohol:
        return Float(bottleNumber!)
    }
}


