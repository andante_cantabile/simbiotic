//
//  BeerModel.swift
//  simbiotic
//
//  Created by Ellen Roper on 12/3/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import Foundation
import ARKit
import SceneKit

class BeerBottle: SCNNode {
    func loadModel() {
        guard let virtualObjectScene = SCNScene(named: "art.scnassets/Corona.scn") else { return }
        let wrapperNode = SCNNode()
        wrapperNode.name = "bottle"
        for child in virtualObjectScene.rootNode.childNodes {
            wrapperNode.addChildNode(child)
        }
        addChildNode(wrapperNode)
    }
}
class Trash: SCNNode {
    func loadModel() {
        guard let virtualObjectScene = SCNScene(named: "art.scnassets/Korsina_trash_paper.dae") else { return }
        let wrapperNode = SCNNode()
        wrapperNode.name = "trash"
        for child in virtualObjectScene.rootNode.childNodes {
            wrapperNode.addChildNode(child)
        }
        addChildNode(wrapperNode)
    }
}
class Pizza: SCNNode {
    func loadModel() {
        guard let virtualObjectScene = SCNScene(named: "art.scnassets/Pizza.obj") else { return }
        let wrapperNode = SCNNode()
        wrapperNode.name = "pizza"
        for child in virtualObjectScene.rootNode.childNodes {
            wrapperNode.addChildNode(child)
        }
        addChildNode(wrapperNode)
    }
}


