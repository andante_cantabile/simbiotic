//
//  RoommateAgreementsViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 12/7/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

var arExperienceDone = false

class RoommateAgreementsViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBAction func goBack(_ sender: Any) {
        if arExperienceDone {
            performSegue(withIdentifier: "goHomeWithAR", sender: self)
        }
        else {
            performSegue(withIdentifier: "goHomeWithoutAR", sender: self)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.layer.cornerRadius = 5.0
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
