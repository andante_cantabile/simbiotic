//
//  QuestionnaireViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/28/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class QuestionnaireViewController: UIViewController {
    @IBOutlet weak var roomCleaninessSlider: UISlider!
    @IBOutlet weak var alchoholSlider: UISlider!
    @IBOutlet weak var smokeSlider: UISlider!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var roomCleaninessLabel: UILabel!
    @IBOutlet weak var alchoholLabel: UILabel!
    @IBOutlet weak var smokeLabel: UILabel!
    var hygieneNumber: Float!
    var responsibilityNumber: Float!
    var alcoholNumber: Float!
    @IBAction func roomCleaninessSliderValueChanged(_ sender: UISlider) {
        let value = Int(sender.value)
        sender.value = Float(value)
        roomCleaninessLabel.text = String(value)
    }
    @IBAction func alchoholSliderValueChanged(_ sender: UISlider) {
        let value = Int(sender.value)
        sender.value = Float(value)
        alchoholLabel.text = String(value)
    }
    @IBAction func smokeSliderValueChanged(_ sender: UISlider) {
        let value = Int(sender.value)
        sender.value = Float(value)
        smokeLabel.text = String(value)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        finishButton.layer.cornerRadius = 5.0
        roomCleaninessSlider.value = Float(scaleNumberToSliderRange(number: hygieneNumber))
        alchoholSlider.value = Float(scaleNumberToSliderRange(number: alcoholNumber))
        smokeSlider.value = Float(scaleNumberToSliderRange(number: responsibilityNumber))
        roomCleaninessLabel.text = String(Int(roomCleaninessSlider.value))
        alchoholLabel.text = String(Int(alchoholSlider.value))
        smokeLabel.text = String(Int(smokeSlider.value))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scaleNumberToSliderRange(number: Float) -> Int {
        return Int(round((10.0 / 3.0) * number))
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! SecondHomeViewController
        destination.hygieneNumber = hygieneNumber
        destination.alcoholNumber = alcoholNumber
        destination.responsibilityNumber = responsibilityNumber
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
