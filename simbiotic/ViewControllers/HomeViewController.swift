//
//  HomeViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/26/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var resourcesButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        startButton.layer.cornerRadius = 5.0
        resourcesButton.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //startButton.layer.shadowColor = UIColor.black.cgColor
        //startButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        //startButton.layer.shadowRadius = 5
        //startButton.layer.shadowOpacity = 0.5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
