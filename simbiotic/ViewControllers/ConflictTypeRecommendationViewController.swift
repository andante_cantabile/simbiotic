//
//  ConflictTypeRecommendationViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 12/7/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class ConflictTypeRecommendationViewController: UIViewController {
    @IBOutlet weak var conflictTypeLabel: UILabel!
    var conflictType: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        conflictTypeLabel.text = conflictType
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
