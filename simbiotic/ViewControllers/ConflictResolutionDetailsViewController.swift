//
//  ConflictResolutionDetailsViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/30/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class ConflictResolutionDetailsViewController: UIViewController {
    @IBOutlet weak var typeTitle: UILabel!
    @IBOutlet weak var typeDescription: UILabel!
    @IBOutlet weak var pro: UILabel!
    @IBOutlet weak var con: UILabel!
    @IBOutlet weak var firstAppropriateUse: UILabel!
    @IBOutlet weak var secondAppropriateUse: UILabel!
    @IBOutlet weak var thirdAppropriateUse: UILabel!
    var appropriateUses: [String]!
    var typeTitleString: String!
    var typeDescriptionString: String!
    var proString: String!
    var conString: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        typeTitle.text = typeTitleString
        typeDescription.text = typeDescriptionString
        pro.text = proString
        con.text = conString
        firstAppropriateUse.text = appropriateUses[0]
        secondAppropriateUse.text = appropriateUses[1]
        thirdAppropriateUse.text = appropriateUses[2]
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleSwipe(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            performSegue(withIdentifier: "showConflictExamples", sender: nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showConflictExamples" {
            let destination = segue.destination as! ConflictExamplesViewController
            destination.type = typeTitle.text
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
