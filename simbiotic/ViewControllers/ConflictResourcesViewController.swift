//
//  ConflictResourcesViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/30/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class ConflictResolutionExamplesViewController: UIViewController {
    @IBOutlet weak var conflictStyleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
