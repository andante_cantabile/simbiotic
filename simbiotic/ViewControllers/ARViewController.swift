//
//  ARViewController.swift
//  simbiotic
//
//  Created by Vrinda V on 12/3/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit
import ARKit

enum Scenario {
    case Bottle
    case Trash
    case Pizza
}

func getQuestionTextOffset(scenario: Scenario) -> Float {
    switch scenario {
    case .Bottle:
        return 0.2
    case .Trash:
        return 0.4
    case .Pizza:
        return 0.1
    }
}

let distance = Float(0.2)

func getSimbioticBlue(alpha: CGFloat) -> UIColor {
    return UIColor(red: 0.22, green: 0.67, blue: 1, alpha: alpha)
}

class ARViewController: UIViewController {
    // An integer to keep track of which item we should place next
    var itemToPlace: Int!
    var readyToDisplayBottle = false
    var readyToDisplayTrash = false
    var readyToDisplayPizza = false
    var tablePlaneAnchor: ARPlaneAnchor!
    var trashPlaneAnchor: ARPlaneAnchor!
    var pizzaPlaneAnchor: ARPlaneAnchor!
    var bottlePosition: SCNVector3!
    var trashPosition: SCNVector3!
    var pizzaPosition: SCNVector3!
    var bottleQuestionTextNode: SCNNode!
    var trashQuestionTextNode: SCNNode!
    var pizzaQuestionTextNode: SCNNode!
    var selectedAnswers = [Scenario: Int]() //NEW: added structure to keep track of which answers the user selects
    var bottleTapped = false
    var trashTapped = false
    var pizzaTapped = false
    var allTextNodes = [SCNNode()] //NEW: added structure to keep track of question AND answer text nodes
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var userPrompt: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBAction func startButtonTapped(_ sender: Any) {
        if selectedAnswers.count == 3 {
            performSegue(withIdentifier: "VRToStanfordLogin", sender: nil)
        }
        switch itemToPlace {
        case 0:
            readyToDisplayBottle = true
            addBottle(anchor: tablePlaneAnchor)
            startButton.isEnabled = false
            startButton.setTitle("Searching...", for: .normal)
            startButton.backgroundColor = getSimbioticBlue(alpha: 0.3)
            startButton.titleLabel?.textColor = UIColor.gray
            userPrompt.text = "Next, point your phone at the ground."
            addLabelsToObjects(object: "Bottle")
            break
        case 1:
            readyToDisplayTrash = true
            addTrash(anchor: trashPlaneAnchor)
            startButton.isEnabled = false
            startButton.setTitle("Searching...", for: .normal)
            startButton.backgroundColor = getSimbioticBlue(alpha: 0.3)
            userPrompt.text = "Now, point at another spot."
            addLabelsToObjects(object: "Trash")
            break
        case 2:
            readyToDisplayPizza = true
            addPizza(anchor: pizzaPlaneAnchor)
            startButton.isEnabled = false
            startButton.setTitle("Searching...", for: .normal)
            startButton.backgroundColor = getSimbioticBlue(alpha: 0.3)
            userPrompt.text = "Now, tap on items that you see to explore."
            addLabelsToObjects(object: "Pizza")
            startButton.isHidden = true
            //addLabelsToObjects()
            break
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemToPlace = 0
        sceneView.debugOptions = ARSCNDebugOptions.showWorldOrigin
        startButton.isEnabled = false
        startButton.setTitle("Searching...", for: .normal)
        startButton.titleLabel?.textColor = UIColor.gray
        startButton.backgroundColor = getSimbioticBlue(alpha: 0.3)
        //sceneView.allowsCameraControl = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(rec:)))
        sceneView.addGestureRecognizer(tap)
        //addTapGestureToSceneView()
        // Do any additional setup after loading the view.
        setupScene()
        // userPrompt.text = "To begin, point your camera to a table"
        startButton.layer.cornerRadius = 7.0
    }
    
    func computeAnswerPositions(textNode: SCNNode) -> [SCNVector3] {
        let leftPosition = SCNVector3(textNode.position.x - 0.235, textNode.position.y, textNode.position.z)
        let topLeftPosition = SCNVector3(textNode.position.x - 0.125, textNode.position.y + 0.15, textNode.position.z)
        let topRightPosition = SCNVector3(textNode.position.x + 0.125, textNode.position.y + 0.15, textNode.position.z)
        let rightPosition = SCNVector3(textNode.position.x + 0.235, textNode.position.y, textNode.position.z)
        return [leftPosition, topLeftPosition, topRightPosition, rightPosition]
    }
    
    func addAnswerText(questionTextNode: SCNNode, scenario: Scenario) {
        // print("Adding answer text")
        let answerPositions = computeAnswerPositions(textNode: questionTextNode)
        let answers = getScenarioContent(scenario: scenario)["Answers"] as! [String]
        for (index, pos) in answerPositions.enumerated() {
            let _ = addText(position: pos, string: answers[index], isQuestion: false, scenario: scenario)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupConfiguration()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupScene() {
        let scene = SCNScene()
        sceneView.scene = scene
    }
    
    func setupConfiguration() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
        sceneView.delegate = self
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }

    func addBottle(anchor:ARPlaneAnchor) {
        let bottle = BeerBottle()
        bottle.loadModel()
        sceneView.scene.rootNode.addChildNode(bottle)
        bottle.position = SCNVector3(anchor.transform.columns.3.x,anchor.transform.columns.3.y, anchor.transform.columns.3.z)
        bottlePosition = bottle.position
        bottle.scale = SCNVector3(0.005,0.005,0.005)
        bottle.eulerAngles = SCNVector3Make(-1.4142, 0, 0)
        itemToPlace += 1
        startButton.isEnabled = false
        startButton.titleLabel?.text = "Searching..."
        startButton.titleLabel?.textColor = UIColor.gray
        addManySpheres(position: bottle.position, nameBase: "BotellaBall")
        let firstPosition = SCNVector3(bottle.position.x, bottle.position.y + 0.05, bottle.position.z)
        let secondPosition = SCNVector3(bottle.position.x - 0.02, bottle.position.y + 0.05, bottle.position.z)
        addManySpheres(position: firstPosition, nameBase: "BotellaBall1")
        addManySpheres(position: secondPosition, nameBase: "BotellaBall2")
    }
    func addSphere(position: SCNVector3, nameBase: String, index: Int) {
        let sphere = SCNSphere(radius: 0.02)
        sphere.firstMaterial?.diffuse.contents = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0)
        let sphereNode = SCNNode(geometry: sphere)
        sphereNode.position = position
        sceneView.scene.rootNode.addChildNode(sphereNode)
        sphereNode.name = nameBase + "_" + String(index)
    }
    
    func addManySpheres(position: SCNVector3, nameBase: String) {
        let offset: Float = 0.001
        var distance: Float = 0.001
        let x = position.x
        let y = position.y
        let z = position.z
        var currentIndex = 0
        while distance <= 0.01 {
            addSphere(position: SCNVector3(x + distance, y, z), nameBase: nameBase, index: currentIndex)
            currentIndex += 1
            addSphere(position: SCNVector3(x - distance, y, z), nameBase: nameBase, index: currentIndex)
            currentIndex += 1
            addSphere(position: SCNVector3(x, y - distance, z), nameBase: nameBase, index: currentIndex)
            currentIndex += 1
            addSphere(position: SCNVector3(x, y + distance, z), nameBase: nameBase, index: currentIndex)
            currentIndex += 1
            addSphere(position: SCNVector3(x, y, z + distance), nameBase: nameBase, index: currentIndex)
            currentIndex += 1
            addSphere(position: SCNVector3(x, y, z - distance), nameBase: nameBase, index: currentIndex)
            currentIndex += 1
            distance += offset
        }
    }
    
    func addPizza(anchor:ARPlaneAnchor) {
        let pizza = Pizza()
        pizza.loadModel()
        // bottle.rotation = SCNVector4(0, 0, 0.7071, 0.7071)
        sceneView.scene.rootNode.addChildNode(pizza)
        //bottle.simdTransform = anchor.transform
        pizza.position = SCNVector3(anchor.transform.columns.3.x,anchor.transform.columns.3.y, anchor.transform.columns.3.z)
        pizzaPosition = pizza.position
        pizza.scale = SCNVector3(0.012,0.012,0.012)
        pizza.eulerAngles = SCNVector3Make(-1.4142, 0, 0)
        itemToPlace += 1
        startButton.isEnabled = false
        startButton.titleLabel?.text = "Searching..."
        startButton.titleLabel?.textColor = UIColor.gray
        addManySpheres(position: pizza.position, nameBase: "PepperoniBall")
    }
    
    func addTrash(anchor:ARPlaneAnchor) {
        let trash = Trash()
        trash.loadModel()
        // bottle.rotation = SCNVector4(0, 0, 0.7071, 0.7071)
        sceneView.scene.rootNode.addChildNode(trash)
        //bottle.simdTransform = anchor.transform
        trash.position = SCNVector3(anchor.transform.columns.3.x,anchor.transform.columns.3.y, anchor.transform.columns.3.z)
        trashPosition = trash.position
        trash.scale = SCNVector3(0.005,0.005,0.005)
        trash.eulerAngles = SCNVector3Make(0, 0, 0)
        itemToPlace += 1
        addManySpheres(position: trash.position, nameBase: "meshBall")
        DispatchQueue.main.async {
            self.startButton.isEnabled = false
            self.startButton.titleLabel?.text = "Searching..."
            self.startButton.titleLabel?.textColor = UIColor.gray
        }
    }
    
    func addLabelsToObjects(object: String) {
        if object == "Bottle" {
            let bottleQuestion = getScenarioContent(scenario: .Bottle)["Question"] as! String
            addQuestionText(objectPosition: bottlePosition, questionString: bottleQuestion, scenario: .Bottle)
        }
        else if object == "Trash" {
            let trashQuestion = getScenarioContent(scenario: .Trash)["Question"] as! String
            addQuestionText(objectPosition: trashPosition, questionString: trashQuestion, scenario: .Trash)
        }
        else {
            let pizzaQuestion = getScenarioContent(scenario: .Pizza)["Question"] as! String
            addQuestionText(objectPosition: pizzaPosition, questionString: pizzaQuestion, scenario: .Pizza)
        }
    }
    
    func addText(position: SCNVector3, string: String, isQuestion: Bool, scenario: Scenario) -> SCNNode {
        let text = SCNText(string: string, extrusionDepth: 0.1)
        let backgroundColor = isQuestion ? getSimbioticBlue(alpha: 0.9) : UIColor.black.withAlphaComponent(0.9)
        text.font = UIFont(name: "Comfortaa-Regular", size: 2.0)
        text.flatness = 0.01
        text.firstMaterial?.diffuse.contents = UIColor.white
        text.containerFrame = CGRect(origin: .zero, size: CGSize(width: 20, height: 20))
        text.isWrapped = true
        text.truncationMode = kCAAlignmentCenter
        text.alignmentMode = kCAAlignmentCenter
        //static let center: CATextLayerAlignmentMode
        let textNode = SCNNode(geometry: text)
        let fontSize = Float(0.01) //maybe use this to change font size
        textNode.scale = SCNVector3(fontSize, fontSize, fontSize)
        sceneView.scene.rootNode.addChildNode(textNode)
        var minVec = SCNVector3Zero
        var maxVec = SCNVector3Zero
        (minVec, maxVec) = textNode.boundingBox
        textNode.pivot = SCNMatrix4MakeTranslation(minVec.x + (maxVec.x - minVec.x)/2, minVec.y, minVec.z + (maxVec.z - minVec.z)/2)
        textNode.position = position
        let bound = SCNVector3Make(maxVec.x - minVec.x,
                                   maxVec.y - minVec.y,
                                   maxVec.z - minVec.z);
        
        let plane = SCNPlane(width: CGFloat(bound.x + 1),
                             height: CGFloat(bound.y + 1))
        plane.cornerRadius = 0.2
        plane.firstMaterial?.diffuse.contents = backgroundColor
        
        let planeNode = SCNNode(geometry: plane)
        planeNode.position = SCNVector3(
            CGFloat( minVec.x) + CGFloat(bound.x) / 2 ,
            CGFloat( minVec.y) + CGFloat(bound.y) / 2,
            CGFloat(minVec.z - 0.01)
        )
        textNode.addChildNode(planeNode)
        switch scenario {
        case .Bottle:
            if !isQuestion {
                textNode.name = "Bottle:" + string
                planeNode.name = "Bottle:" + string + "_plane"
            } else {
                textNode.name = "Bottle Question"
                planeNode.name = "Bottle plane"
            }
        case .Trash:
            if !isQuestion {
                textNode.name = "Trash:" + string
                planeNode.name = "Trash:" + string + "_plane"
            } else {
                textNode.name = "Trash Question"
                planeNode.name = "Trash plane"
            }
        case .Pizza:
            if !isQuestion {
                textNode.name = "Pizza:" + string
                planeNode.name = "Pizza:" + string + "_plane"
            } else {
                textNode.name = "Pizza Question"
                planeNode.name = "Pizza plane"
            }
        }
        //textNode.name = scenario.rawValue + ": " + string //new
        allTextNodes.append(textNode) //new
        allTextNodes.append(planeNode)
        return textNode
    }
    
    func addQuestionText(objectPosition: SCNVector3, questionString: String, scenario: Scenario) {
        let questionTextOffset = getQuestionTextOffset(scenario: scenario)
        let updatedPosition = SCNVector3(objectPosition.x, objectPosition.y + questionTextOffset, objectPosition.z)
        let node = addText(position: updatedPosition, string: questionString, isQuestion: true, scenario: scenario)
        switch scenario {
        case .Bottle:
            bottleQuestionTextNode = node
        case .Trash:
            trashQuestionTextNode = node
        case .Pizza:
            pizzaQuestionTextNode = node
        }
    }
}
extension ARViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        // 2
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        let plane = SCNPlane(width: width, height: height)
        //let plane = SCNPlane(width: 0.01, height: 0.01)
        
        // 3
        //plane.materials.first?.diffuse.contents = UIColor.green
        
        // 4
        let planeNode = SCNNode(geometry: plane)
        
        // 5
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x,y,z)
        planeNode.eulerAngles.x = -.pi / 2
        DispatchQueue.main.async {
            self.startButton.isEnabled = true
            self.startButton.setTitle("Go!", for: .normal)
            self.startButton.titleLabel?.textColor = UIColor.white
            self.startButton.backgroundColor = getSimbioticBlue(alpha: 0.9)
        }
        
        if(itemToPlace == 0) {
            if (readyToDisplayBottle) {
                addBottle(anchor:planeAnchor)
            }
            else {
                tablePlaneAnchor = anchor as? ARPlaneAnchor
            }
            return
        }
        if(itemToPlace == 1) {
            if (readyToDisplayTrash) {
                addTrash(anchor:planeAnchor)
            }
            else {
                trashPlaneAnchor = anchor as? ARPlaneAnchor
            }
            return
        }
        if(itemToPlace == 2) {
            if (readyToDisplayPizza) {
                addPizza(anchor: planeAnchor)
            }
            else {
                pizzaPlaneAnchor = anchor as? ARPlaneAnchor
            }
            return
        }
    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
            let planeNode = node.childNodes.first,
            let plane = planeNode.geometry as? SCNPlane
            else { return }
        
        // 2
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        plane.width = width
        plane.height = height
        
        // 3
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x, y, z)
    }
    
    @objc func handleTap(rec: UITapGestureRecognizer){
        if rec.state == .ended {
            let location: CGPoint = rec.location(in: sceneView)
            let hits = self.sceneView.hitTest(location,options: nil)
            if !hits.isEmpty {
                let tappedNode = hits.first?.node
                // print(tappedNode?.name as Any)
                //let detectedText = self.allTextNodes[tappedNode!] //new
                //print(detectedText?.name as Any) //new
                if let nodeName = tappedNode?.name {
                    if nodeName.range(of: "Botella") != nil && !bottleTapped {
                        // The bottle is tapped
                        addAnswerText(questionTextNode: bottleQuestionTextNode, scenario: .Bottle)
                        bottleTapped = true
                    }
                    else if nodeName.range(of: "mesh") != nil && !trashTapped {
                        // The trash can is tapped
                        addAnswerText(questionTextNode: trashQuestionTextNode, scenario: .Trash)
                        trashTapped = true
                    }
                    else if nodeName.range(of: "Pepperoni") != nil && !pizzaTapped {
                        // The pizza is tapped
                        addAnswerText(questionTextNode: pizzaQuestionTextNode, scenario: .Pizza)
                        pizzaTapped = true
                    }
                    else if nodeName.range(of: "Bottle:") != nil {
                        // An answer to the bottle question is tapped
                        let answerText = extractAnswerStringFromNodeName(string: nodeName)
                        let answerIndex = translateStringToAnswerIndex(scenario: .Bottle, string: answerText)
                        assert(answerIndex >= 0)
                        selectedAnswers[.Bottle] = answerIndex
                        for node in allTextNodes {
                            if node.name?.range(of: "Bottle") != nil {
                                node.removeFromParentNode()
                            }
                        }
                        let _ = addQuestionText(objectPosition: bottlePosition, questionString: "Done!", scenario: .Bottle)
                        print(selectedAnswers.count)
                        if selectedAnswers.count == 3 {
                            print("VR Done!")
                            startButton.isHidden = false
                            startButton.setTitle("Finish", for: .normal)
                            startButton.backgroundColor = getSimbioticBlue(alpha: 0.9)
                            startButton.isEnabled = true
                        }
                    }
                    else if nodeName.range(of: "Trash:") != nil {
                        // An answer to the trash can question is tapped
                        let answerText = extractAnswerStringFromNodeName(string: nodeName)
                        let answerIndex = translateStringToAnswerIndex(scenario: .Trash, string: answerText)
                        assert(answerIndex >= 0)
                        selectedAnswers[.Trash] = answerIndex
                        for node in allTextNodes {
                            if node.name?.range(of: "Trash") != nil {
                                node.removeFromParentNode()
                            }
                        }
                        let _ = addQuestionText(objectPosition: trashPosition, questionString: "Done!", scenario: .Trash)
                        print(selectedAnswers.count)
                        if selectedAnswers.count == 3 {
                            print("VR Done!")
                            userPrompt.text = "Your AR experience is now complete!"
                            startButton.isHidden = false
                            startButton.setTitle("Finish", for: .normal)
                            startButton.backgroundColor = getSimbioticBlue(alpha: 0.9)
                            startButton.isEnabled = true
                        }
                    }
                    else if nodeName.range(of: "Pizza:") != nil {
                        // An answer to the pizza question is tapped
                        let answerText = extractAnswerStringFromNodeName(string: nodeName)
                        let answerIndex = translateStringToAnswerIndex(scenario: .Pizza, string: answerText)
                        selectedAnswers[.Pizza] = answerIndex
                        for node in allTextNodes {
                            if node.name?.range(of: "Pizza") != nil {
                                node.removeFromParentNode()
                            }
                        }
                        let _ = addQuestionText(objectPosition: pizzaPosition, questionString: "Done!", scenario: .Pizza)
                        print(selectedAnswers.count)
                        if selectedAnswers.count == 3 {
                            print("VR Done!")
                            startButton.isHidden = false
                            startButton.setTitle("Finish", for: .normal)
                            startButton.backgroundColor = getSimbioticBlue(alpha: 0.9)
                            startButton.isEnabled = true
                        }
                    }
                        //Add the selected node text to global array of selected answers.
                        //call some function to delete/hide the nodes of all values in the allTextNodes that start with "Bottle: " so that we know that the question/answers should disappear after being tapped
                }
                    //else if nodeName.range(of:)
            }
               // if detectedText.
        }
    }
    
    func extractAnswerStringFromNodeName(string: String) -> String {
        let substring = String(string.split(separator: ":")[1])
        let answerText = substring.split(separator: "_")[0]
        return String(answerText)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        arExperienceDone = true
        let destination = segue.destination as! StanfordLoginViewController
        destination.answers = self.selectedAnswers
    }
}
//}
