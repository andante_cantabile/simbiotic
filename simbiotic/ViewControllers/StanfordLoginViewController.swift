//
//  StanfordLoginViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/28/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class StanfordLoginViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    var answers: [Scenario: Int]!
    var surveyAnswers = [SurveyQuestion: Float]()
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.layer.cornerRadius = 3.0
        passwordTextField.layer.cornerRadius = 3.0
        loginButton.layer.cornerRadius = 5.0
        print(answers)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        print(answers)
        surveyAnswers[.Alcohol] = computeSurveyAnswer(vrAnswers: answers, question: .Alcohol)
        surveyAnswers[.ShareResponsibility] = computeSurveyAnswer(vrAnswers: answers, question: .ShareResponsibility)
        surveyAnswers[.Hygiene] = computeSurveyAnswer(vrAnswers: answers, question: .Hygiene)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! QuestionnaireViewController
        destination.hygieneNumber = surveyAnswers[.Hygiene]
        destination.alcoholNumber = surveyAnswers[.Alcohol]
        destination.responsibilityNumber = surveyAnswers[.ShareResponsibility]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
