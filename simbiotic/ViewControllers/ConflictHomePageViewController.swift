//
//  ConflictHomePageControllerViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 12/7/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

enum ConflictStyle {
    case Avoid
    case Compete
    case Compromise
    case Concede
    case Collaborate
}

func getConflictStyleText(style: ConflictStyle) -> String {
    switch style {
    case .Avoid:
        return "Avoid"
    case .Compete:
        return "Compete"
    case .Compromise:
        return "Compromise"
    case .Concede:
        return "Concede"
    case .Collaborate:
        return "Collaborate"
    }
    
}

class ConflictHomePageViewController: UIViewController {
    var hygieneNumber: Float!
    var responsibilityNumber: Float!
    var alcoholNumber: Float!
    var style: ConflictStyle!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.layer.cornerRadius = 5.0
        backButton.layer.cornerRadius = 5.0
        if hygieneNumber == 0 || responsibilityNumber == 0 || alcoholNumber == 0{
            style = .Avoid
        } else if alcoholNumber == 3 || responsibilityNumber == 3 {
            style = .Compete
        } else if alcoholNumber == 2 || responsibilityNumber == 2 {
            style = .Compromise
        } else if alcoholNumber == 1 || responsibilityNumber == 1 || hygieneNumber == 1 {
            style = .Concede
        } else {
            style = .Collaborate
        }
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ConflictTypeRecommendationViewController
        destination.conflictType = getConflictStyleText(style: style)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
