//
//  ConflictResolutionExamplesViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/30/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class ConflictExamplesViewController: UIViewController {
    var type: String!
    @IBOutlet weak var typeTitle: UILabel!
    @IBOutlet weak var typeDescription: UILabel!
    @IBOutlet weak var goal: UILabel!
    @IBOutlet weak var firstExample: UILabel!
    @IBOutlet weak var secondExample: UILabel!
    @IBOutlet weak var thirdExample: UILabel!
    @IBOutlet weak var logo: UIImageView!
    var information: [String: Any]!
    override func viewDidLoad() {
        super.viewDidLoad()
        typeTitle.text = type
        self.information = typeInformation[type]!
        if let descriptionString = self.information["description"] as! String? {
            typeDescription.text = descriptionString
        }
        if let goalString = self.information["goal"] as! String? {
            goal.text = "Goal: " + goalString
        }
        if let examples = self.information["examples"] as! [String]? {
            firstExample.text = examples[0]
            secondExample.text = examples[1]
            thirdExample.text = examples[2]
        }
        if let imageName = self.information["imageName"] as! String? {
            logo.image = UIImage(named: imageName)
        }
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleSwipe(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == UISwipeGestureRecognizerDirection.left {
            performSegue(withIdentifier: "showConflictDetails", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showConflictDetails" {
            let destination = segue.destination as! ConflictResolutionDetailsViewController
            destination.typeTitleString = typeTitle.text
            if let appropriateUses = self.information["appropriateUses"] as! [String]? {
                destination.appropriateUses = appropriateUses
            }
            if let pro = self.information["pro"] as! String? {
                destination.proString = "Pro: " + pro
            }
            if let con = self.information["con"] as! String? {
                destination.conString = "Con: " + con
            }
            if let description = self.information["description"] as! String? {
                destination.typeDescriptionString = description
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
