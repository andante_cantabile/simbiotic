//
//  ConflictManagementTypesViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/30/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class ConflictManagementTypesViewController: UIViewController {
    var conflictManagementStyle: String!
    @IBAction func buttonTapped(_ sender: UIButton) {
        conflictManagementStyle = sender.titleLabel?.text
    }
    @IBOutlet weak var backButton: UIButton!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goBack" {
            return
        }
        let destination = segue.destination as! ConflictExamplesViewController
        destination.type = self.conflictManagementStyle.trimmingCharacters(in: .whitespaces)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.layer.cornerRadius = 5.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
