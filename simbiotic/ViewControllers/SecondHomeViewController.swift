//
//  SecondHomeViewController.swift
//  simbiotic
//
//  Created by Yuguan Xing on 11/29/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class SecondHomeViewController: UIViewController {
    @IBOutlet weak var ConflictResButton: UIButton!
    @IBOutlet weak var RoommateTopicsButton: UIButton!
    var hygieneNumber: Float!
    var responsibilityNumber: Float!
    var alcoholNumber: Float!
    override func viewDidLoad() {
        super.viewDidLoad()
        ConflictResButton.layer.cornerRadius = 5.0
        RoommateTopicsButton.layer.cornerRadius = 5.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "secondHomeToConflictResolution" {
            let destination = segue.destination as! ConflictHomePageViewController
            destination.hygieneNumber = hygieneNumber
            destination.alcoholNumber = alcoholNumber
            destination.responsibilityNumber = responsibilityNumber
            print(hygieneNumber)
            print(alcoholNumber)
            print(responsibilityNumber)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
