//
//  ConflictHomePageController.swift
//  simbiotic
//
//  Created by Ellen Roper on 12/6/18.
//  Copyright © 2018 Yuguan Xing. All rights reserved.
//

import UIKit

class ConflictHomePageController: UIViewController {
    var hygieneNumber: Float!
    var responsibilityNumber: Float!
    var alcoholNumber: Float!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! ConflictResolutionExamplesViewController
        destination.hygieneNumber = hygieneNumber
        destination.alcoholNumber = alcoholNumber
        destination.responsibilityNumber = responsibilityNumber
        print(hygieneNumber)
        print(alcoholNumber)
        print(responsibilityNumber)
    }
}
